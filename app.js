const express = require('express');
const app = express();
const PORT = 3000;

// GET ENDPOINT - http://127.0.0.1:3000/
app.get('/', (req, res) => {
  res.send('Hello world!');
});

/**
 * This function multiplies multiplicant with multiplier.
 * @param {number} multiplicant first parameter
 * @param {number} multiplier second parameter
 * @returns {number} product
 */
const multiply = (multiplicant, multiplier) => {
  const product = multiplicant * multiplier;
  return product;
};

// GET multiply ENDPOINT - http://127.0.0.1:3000/multiply?a=3b=5
app.get('/multiply', (req, res) => {
  try {
    const multiplicant = parseFloat(req.query.a);
    const multiplier = parseFloat(req.query.b);

    if (isNaN(multiplicant)) throw new Error(`Invalid multiplier value`)
    if (isNaN(multiplier)) throw new Error(`Invalid multiplicant value`)
    
    console.log({ multiplicant, multiplier });
    const product = multiply(multiplicant, multiplier);
    res.send(product.toString(10));
  } catch (error) {
    console.error(error);
    res.send('Could not calculate the product. Try again.');
  }
});

app.listen(PORT, () => {
  console.log(`Server is listening on http://127.0.0.1:${PORT}`);
});
